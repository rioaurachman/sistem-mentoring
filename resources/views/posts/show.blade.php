@extends('rotating_card.master')

@section('content')
    <div class="mt-3 ml-3">
        {{-- <h4>{{$post->title}}</h4> --}}
        {{-- <p>{{$post->content}}</p> --}}
        {{-- <p>{{$post->picture}}</p> --}}
        {{-- <p>{{$post->quote}}</p> --}}
        {{-- <p>Posting Dari: {{$post->author->name}}</p> --}}
        {{-- <p>Jumlah Like: {{$jumlahlike}}</p> --}}
    </div>

    <div class="col-sm-10 col-sm-offset-1">
        <div class="col-md-4 col-sm-6">
         <div class="card-container">
            <div class="card">
                <div class="front">
                    <div class="cover">
                        <img src="{{asset('/rotating_card/images/rotating_card_thumb.png')}}"/>
                    </div>
                    <div class="user">
                        <img class="img-circle" src="{{asset('/rotating_card/images/rotating_card_profile2.png')}}"/>
                    </div>
                    <div class="content">
                        <div class="main">
                            <h3 class="name">{{$post->author->name}}</h3>
                            <p class="profession">Web Development Junior</p>
                            <p class="text-center">"Saya adalah programmer pemula yang sedang belajar di Sanbercode"</p>
                        </div>
                        <div class="footer">
                            <i class="fa fa-mail-forward"></i> Auto Rotation
                        </div>
                    </div>
                </div> <!-- end front panel -->
                <div class="back">
                    <div class="header">
                        <h4 class="text-center">{{$post->title}}</h4>
                        <p class="text-center">{{$post->content}}</p>

                    </div>
                    <div class="content">
                        <div class="main">
                            <h5 class="motto">"{{$post->quote}}"</h5>

                            <div class="stats-container">
                                <div class="stats">
                                    <h4>235</h4>
                                    <p>
                                        Followers
                                    </p>
                                </div>
                                <div class="stats">
                                    <h4>114</h4>
                                    <p>
                                        Following
                                    </p>
                                </div>
                                <div class="stats">
                                    <h4>{{$jumlahlike}}</h4>
                                    <p>
                                        Like
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-6"> 
                        <a href="#" class="btn btn-primary btn-block">
                            <i class="fa fa-user-plus"></i> Follow</a> 
                    </div>                 
                    <div class="col-xs-6"> 
                        <a href="#" class="btn btn-success btn-block">
                            <i class="fa fa-heart"></i> Like</a>
                    </div>

                    <div class="footer">
                        <div class="social-links text-center">
                            <a href="https://creative-tim.com" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                            <a href="https://creative-tim.com" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                            <a href="https://creative-tim.com" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                        </div>
                    </div>
                </div> <!-- end back panel -->
            </div> <!-- end card -->
        </div> <!-- end card-container -->
        </div> <!-- end col sm 3 -->
    </div> <!-- end col-sm-10 -->
    </div> <!-- end row -->
@endsection