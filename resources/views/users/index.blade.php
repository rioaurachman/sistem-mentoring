@extends('rotating_card.master')

@section('content')

    <div class="mt-3 ml-3">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if(session('success'))
                <div class="alert alert-sucess">
                    {{session('success')}}
                </div>
             @endif
            
              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th style="width: 40px">Label</th>
                </tr>
    
              </thead>
              <tbody>
                @forelse($users as $key =>$user)
                  <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$user->name}} </td>
                    <td> {{$user->email}} </td>
                    <td style="display: flex;">
                        <a href="/users/{{$user->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/users/{{$user->id}}/edit" class="btn btn-info btn-sm">edit</a>
                        <form action="/users/{{$user->id}}" method="post">
                          @csrf
                          @method('DELETE')
                             <button class="btn btn-danger btn-blok">
                             <i class="fa fa-trash"></i>
                             </button>
                         </form>
                    </td>
                  </tr>
                @empty

                    <tr>
                    <p>No Users</p>
                    </tr>
                @endforelse

              </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
    </div>
@endsection