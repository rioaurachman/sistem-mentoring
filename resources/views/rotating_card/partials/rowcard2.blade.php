@extends('rotating_card.master')

@section('content')

    <div class="row">
    <h1 class="title">
        Social Media
        <br>
        <small>Sanbercode Laravel Bootcamp #10</small>
    </h1>
    <div class="col-sm-10 col-sm-offset-1">
        @forelse($users as $key =>$user)
            <div class="col-md-4 col-sm-6">
            <div class="card-container">
                <div class="card">
                    <div class="front">
                        <div class="cover">
                            <img src="https://picsum.photos/{{rand(100,200)}}"/>
                        </div>
                        <div class="user">
                            <img class="img-circle" src="https://picsum.photos/{{rand(10,100)}}"/>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h3 class="name">{{$user->name}}</h3>
                                <p class="profession">{{$user->email}}</p>
                                <p class="text-center">"I'm the new Sinatra, and since I made it here I can make it anywhere, yeah, they love me everywhere"</p>
                            </div>
                            <div class="footer">
                                <i class="fa fa-mail-forward"></i> Auto Rotation
                            </div>
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto">"To be or not to be, this is my awesome motto!"</h5>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center">Email</h4>
                                <p class="text-center">{{$user->email}}</p>

                                <div class="stats-container">
                                    <div class="stats">
                                        <h4>{{DB::table('user_follow_users')->where('followed_id',$user->id)->distinct('follower_id')->count('follower_id')}}</h4>
                                        {{--hitung jumlah follower {{DB::table('user_follow_users')->where('followed_id',$user->id)->count()}}--}}
                                        {{--silahkan pilih hitung jumlah follower uniq{{DB::table('user_follow_users')->where('followed_id',$user->id)->distinct('follower_id')->count('follower_id')}}--}}
                                        <p>
                                            Followers
                                        </p>
                                    </div>
                                    <div class="stats">
                                        <h4>{{DB::table('user_follow_users')->where('follower_id',$user->id)->distinct('followed_id')->count('followed_id')}}</h4>
                                        {{--hitung jumlah follower {{DB::table('user_follow_users')->where('follower_id',$user->id)->count()}}--}}
                                        {{--silahkan pilih hitung jumlah follower uniq{{DB::table('user_follow_users')->where('follower_id',$user->id)->distinct('followed_id')->count('followed_id')}}--}}
                                         <p>
                                            Following
                                        </p>
                                    </div>
                                    <div class="stats">
                                        <h4>{{DB::table('user_like_posts')->where('user_id',$user->id)->count()}}</h4>
                                        <p>
                                            Like Post
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-xs-6"> 
                            <a href="/follow2/{{$user->id}}/{{Auth::id()}}" class="btn btn-success btn-block">
                                <i class="fa fa-user-plus"></i> Follow</a> 
                        </div>                 
                        <div class="col-xs-6"> 
                            <a href="#" class="btn btn-success btn-block">
                                <i class="fa fa-heart"></i> Like</a>
                        </div>

                        <div class="footer">
                            <div class="social-links text-center">
                                <a href="https://creative-tim.com" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                <a href="https://creative-tim.com" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                <a href="https://creative-tim.com" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
            </div> <!-- end col sm 3 -->
        <!--         <div class="col-sm-1"></div> -->
        @empty

        <tr>
        <p>No Users</p>
        </tr>
        @endforelse

    </div> <!-- end col-sm-10 -->
</div> <!-- end row -->
@endsection

