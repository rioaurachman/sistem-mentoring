@extends('rotating_card.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="/updatepresensi" method="POST">
              @csrf
              <div class="box-body">
              <table class="table-responsive table-hover table-sm">
              <thead>
                  <tr>
                      <th class="name-col">Student Name</th>

                      @foreach ($pekan as $p)
                        <th>{{$p->created_at->format('d M')}}</th>

                      @endforeach
                  </tr>
              </thead>
              <tbody>
                  <p hidden>{{$i=0}}</p>


                  @foreach ($users as $u)
                  <tr class="student">
        
                      <td class="name-col">{{$u->name}}</td>
                      {{--{{'dump($u)'}}--}}
                      
                      @foreach ($pekan as $p)
                      <p hidden>{{$x=DB::table('kehadiran')->where('iduser','=',$u->id)->where('idpekan','=',$p->id)->value('statuskehadiran')}}</p>
                      {{--{{'dump($x')}}--}}
                        <td class="attend-col"><input type="checkbox" name="statuskehadiran[{{$i}}]" value=1 {{$x == 1 ? 'checked' : ''}}></td>
                        <input type="hidden" name="user[{{$i}}]" value="{{$u->id}}">
                        <input type="hidden" name="pekan[{{$i}}]" value="{{$p->id}}">
                        {{--<td><input type="text" class="form-control" id="content" name="statuskehadiran[{{$i}}]" placeholder="Body"></td>--}}
                        <p hidden>{{$i=$i+1}}</p>
                      @endforeach
                  </tr>
                  @endforeach
                  </tbody>
              </table>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
    </div>
    </div>
    @endsection