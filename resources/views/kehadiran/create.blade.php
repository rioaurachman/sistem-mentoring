@extends('rotating_card.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create New User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/users" method="POST">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" name="name" value="{{old('name','')}}" placeholder="insert name">
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" id="email" name="email" value="{{old('email','')}}" placeholder="insert email">
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="text" class="form-control" id="password" name="password" value="{{old('password','')}}" placeholder="enter password">
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="fullname">Full Name</label>
                  <input type="text" class="form-control" id="fullname" name="fullname" value="{{old('fullname','')}}" placeholder="insert full name">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="address">Address</label>
                  <input type="text" class="form-control" id="address" name="address" value="{{old('address','')}}" placeholder="insert address">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="placeofbirth">Place of Birth</label>
                  <input type="text" class="form-control" id="placeofbirth" name="placeofbirth" value="{{old('placeofbirth','')}}" placeholder="insert place of birth"> 
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="dateofbirth">Date of Birth</label>
                  <input type="text" class="form-control" id="dateofbirth" name="dateofbirth" value="{{old('dateofbirth','')}}" placeholder="insert date of birth">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="gender">Gender</label>
                  <input type="text" class="form-control" id="gender" name="gender" value="{{old('gender','')}}" placeholder="insert gender">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create User</button>
              </div>
            </form>
    </div>
</div>
@endsection