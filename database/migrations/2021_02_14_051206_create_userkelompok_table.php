<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserkelompokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userkelompok', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedBigInteger('iduser')->nullable();;
            $table->foreign('iduser')->references('id')->on('users');
            $table->unsignedBigInteger('idkelompok')->nullable();;
            $table->foreign('idkelompok')->references('id')->on('kelompok');
            $table->date('tanggalbergabung')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userkelompok');
    }
}
