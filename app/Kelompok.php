<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelompok extends Model
{
    //
    protected $table = "kelompok";
    protected $fillable =["nama"];

    public function anggota(){
        return $this->hasMany("App\User","idkelompok");
    }
}
