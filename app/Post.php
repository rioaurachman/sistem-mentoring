<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    protected $fillable =["title","content","picture","quote","user_id","comment_id"];
    public function author(){
        return $this->belongsTo('App\user','user_id');
    }
}
