<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('users.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //dd($request->all());
        #$request->validate([
        #    'title'=>'required|unique:posts',
        #    "body"=>'required'
        #]);
        //$query = DB::table('posts')->insert([
        //    "title"=>$request["title"],
        //    "body"=>$request["body"]
        //]);

        #$post = new Post;
        #$post->title = $request["title"];
        #$post->body = $request["body"];
        #$post->save();

        $user = User::create([
            "name" => $request["name"],
            "email" => $request["email"],
            "password" => $request["password"],
            "fullname" => $request["fullname"],
            "address" => $request["address"],
            "placeofbirth" => $request["placeofbirth"],
            "dateofbirth" => $request["dateofbirth"],
            "gender" => $request["gender"]
        ]);

        return redirect('/users')->with('success','User Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = User::where('id',$id)->update([
            "name" => $request["name"],
            "email" => $request["email"],
            "password" => $request["password"],
            "fullname" => $request["fullname"],
            "address" => $request["address"],
            "placeofbirth" => $request["placeofbirth"],
            "dateofbirth" => $request["dateofbirth"],
            "gender" => $request["gender"]
        ]);

        #dd($posts);
        return redirect('/users')->with('success','User Berhasil Disimpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::destroy($id);
        return redirect('/users')->with('success','User Berhasil Dihapus!');
    }

    public function follow($id,$user_id){
        //dd($request->all());
        #$request->validate([
        #    'title'=>'required|unique:posts',
        #    "body"=>'required'
        #]);
        
        #$posts=DB::table('posts')
        #    ->where('id',$id)
        #    ->update([
        #        "title"=>$request["title"],
        #        "body"=>$request["body"]
        #    ]);
        //]);
        #$posts=DB::table('user_like_posts')
            
        #$update = user_like_posts::where('id',$id)->update([
        #            "title" => $request["title"],
        #            "body" => $request["body"]
        $query = DB::table('user_follow_users')->insert([
            "followed_id"=>$id,
            "follower_id"=>$user_id,
            "poin"=>1

        ]);
        return redirect('/users')->with('success','berhasil follow');
        }

        public function followtoprofile($id,$user_id){
            $query = DB::table('user_follow_users')->insert([
                "followed_id"=>$id,
                "follower_id"=>$user_id,
                "poin"=>1
    
            ]);
            return redirect('/profile')->with('success','berhasil follow');
            }

        public function indexprofile()
        {
            $users = User::all();
            return view('rotating_card.partials.rowcard2', compact('users'));
        }


}