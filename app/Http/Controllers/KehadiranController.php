<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\kehadiran;
use App\User;
use App\pekan;
use App\Kelompok;
use Auth;

class KehadiranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        #$post = Post::find($id);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inputpresensi(){
        #nanti bisa ada variabel untuk nyari 
        #$update = kehadiran::where('id',$id)->update([
        #    "title" => $request["title"],
        #    "content" => $request["content"],
        #    "picture" => 1, #$request["picture"],
        #    "quote" => $request["quote"],
         #   "user_id" => Auth::id(), #ini sepertinya bisa dihilangkan
        #    "comment_id" => $request["comment_id"]
        #]);
        #$users = User::all(); #posts nya bisa difilter
        
        #$user1=User::whereIn('id',['3'])->get();
        #$user2=$user1->first()->grup()->get(); #dua baris ini berhasil tarik kelompok dari user
        #$users=$user2->first()->anggota()->get(); #tiga baris ini berhasil tarik anggota lainnya yang sekelompok
        
        $user1=Auth::user();
        $user2=$user1->first()->grup()->get(); #dua baris ini berhasil tarik kelompok dari user
        $users=$user2->first()->anggota()->get(); 

        #$users=User::whereIn('id',['1'])->grup()->anggota()->get();->anggota()->get();

        #$users = kelompok::find(1)->anggota()->get(); #satu baris ini input kelompok berhasil tarik dua anggota
        
        #$kel = $kel2->first()->anggota;
        #$kel = kelompok::find(1);
        #dd($kel2);

        #$users = User::whereIn('id',['1','2','3'])->get();;
        #foreach($users as $u){
        #    dd($u);
        #}
        #$array = explode(',', '1,2,3');
        #$users = User::where('id','array')->first();
        #$c = collect($users);
        #dd($users);

        $pekan = pekan::all();
        
        $kehadiran = kehadiran::all();
        
        #dd($users);

        #dd($posts);
        return view('kehadiran.edit',compact('kehadiran','users','pekan'));
        //
    }

    public function updatepresensi(Request $request)
    {
        #foreach user foreach pekan
        $input = $request->all();
        #$i = $request->input('i');
        $u = $request->input('user');
        $p = $request->input('pekan');
        $s = $request->input('statuskehadiran');
        
        #$key = array_search($si,$s);
        #dump($input);
        #dd($key);
        #dd($s);
        #$i=0;
        #dd($s);
        $i=0; 
        while($i<count($u)){
            if (isset($s[$i])) {
                $x=1;
            } else {
                $x=0;
            }
            $update = kehadiran::where('iduser',$u[$i])->where('idpekan',$p[$i])->update([    
                "statuskehadiran" => $x
            ]);
            $i=$i+1; #kenapa selalu mencapai 64
        }
        #dd($s);
        return redirect('/inputpresensi')->with('success','Post Berhasil Disimpan');
    }
    #inputpresensi


    public function autoisi(){
        $users = User::all();
        $pekan = pekan::all();
        foreach ($users as $u){
            foreach ($pekan as $p){
            $hadir = kehadiran::firstOrCreate([
                "iduser" => $u->id,
                "idpekan" => $p->id
            ]);
            }
        }
        #statuskehadiran();
        return redirect('/inputpresensi')->with('success', 'Post Berhasil Disimpan!');
    }





}
