<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kehadiran extends Model
{
    //
    protected $table = "kehadiran";
    protected $fillable = [
        'iduser','idpekan','statuskehadiran'
    ];
    public function orang(){
        return $this->hasMany('App\User','iduser');
    }

}
