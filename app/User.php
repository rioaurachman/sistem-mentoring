<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $fillable = [
        'name', 'email','password','fullname','address','placeofbirth','dateofbirth','gender','idkelompok'
    ];

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function datang(){
        return $this->hasMany('App\kehadiran');
    }

    public function grup(){
        return $this->belongsTo('App\Kelompok','idkelompok');
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    #protected $hidden = [
    #    'password', 'remember_token',
    #];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    #protected $casts = [
    #    'email_verified_at' => 'datetime',
    #];
}
