<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userkelompok extends Model
{
    //
    protected $table = "userkelompok";
    protected $fillable =["iduser","idkelompok","tanggalbergabung"];
}
