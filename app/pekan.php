<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pekan extends Model
{
    //
    protected $table = "pekan";
    protected $fillable =["awalpekan","akhirpekan"];
}
